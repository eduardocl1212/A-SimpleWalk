package gtosecurity.com.asimplewalk;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class Blogger extends AppCompatActivity {
    private Toolbar commentToolbar;
    ImageView imgGrande;
    TextView txtTitulo;
    TextView txtContenido;
    private FirebaseFirestore firebaseFirestore;
    private FirebaseAuth firebaseAuth;
    private CircleImageView setupImage;
    private Uri mainImageURI = null;
    private String blog_post_id;
    private String current_user_id;
    private int i;
    private String[] title = {"Posts", "Entretenimiento", "Lugares","Rutas", "Restaurantes "};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blogger);
        imgGrande = findViewById(R.id.imgGrande);
        txtTitulo = findViewById(R.id.txtTitulo);
        txtContenido = findViewById(R.id.txtContent);

        Toolbar setupToolbar = findViewById(R.id.setupToolbar2);
        setSupportActionBar(setupToolbar);
        getSupportActionBar().setTitle("Informacion que cura");


        firebaseAuth = FirebaseAuth.getInstance();
        firebaseFirestore = FirebaseFirestore.getInstance();

        current_user_id = firebaseAuth.getCurrentUser().getUid();
        blog_post_id = getIntent().getStringExtra("blog_post_id");

        for (i = 0; i < 5; i++) {
            firebaseFirestore.collection(title[i]).document(blog_post_id).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                @Override
                public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                    if (task.isSuccessful()) {

                        if (task.getResult().exists()) {

                            String name = task.getResult().getString("desc");
                            String image = task.getResult().getString("image_url");
                            String text = task.getResult().getString("longdesc");

                            mainImageURI = Uri.parse(image);

                            txtTitulo.setText(name);
                            txtContenido.setText(text);

                            RequestOptions requestOptions = new RequestOptions();
                            requestOptions.placeholder(R.drawable.image_placeholder);

                            Glide.with(Blogger.this).applyDefaultRequestOptions(requestOptions).load(mainImageURI).thumbnail(
                                    Glide.with(Blogger.this).load(mainImageURI)
                            ).into(imgGrande);


                        } else {
                            //Toast.makeText(Blogger.this, "OOPPSSS", Toast.LENGTH_SHORT).show();
                            i = 0;
                        }

                    }
                }
            });
        }
    }
}

